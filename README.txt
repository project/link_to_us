$Id:

*******************************************************
    README.txt for link_to_us.module for Drupal
*******************************************************

DESCRIPTION
-----------
The Link_to_us module creates a page to display uploaded banners that can be used by others to link to your Drupal site. The module will create well formed SEO links with full title, alt and anchor text determined by the node title, taxonomy term or other pages that are directed to the module. This allows users or writing contributors the ability to use consistent banners to link to your site. Also, link campaigns have a natural page that can be used to establish consistent, well formed links. 

INSTALLATION
------------
1. Place the link_to_us directory in sites/all/modules.

2. Enable this module at Administer -> Site building -> Modules.

3. Go to the link_to_us settings page at Administer -> Site configuration -> Link_to_us

4. Update user permissions if you want other roles to manage the module options.

OPTIONS
-------
Option to add a text link.
Select which node types should have links added



KNOWN ISSUES
------------
None

POSSIBLE FUTURE FEATURES
------------------------
Add greybox, lightbox of thickbox popup ability
Port to Drupal 6.
In Drupal 6, us module popups through AHAH Framework.


MORE INFORMATION
----------------
Sponsor and Demo Site: 
http://www.dog-park-usa.com
http://www.dog-park-usa.com/link_to